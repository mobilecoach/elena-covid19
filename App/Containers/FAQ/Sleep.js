
// TODO for improvement check: https://github.com/idibidiart/react-native-responsive-grid/blob/master/UniversalTiles.md

import React, { Component } from 'react'
import { Text, View, StyleSheet, ScrollView, TouchableOpacity, Linking } from 'react-native'
import ParsedText from 'react-native-parsed-text'
import Collapsible from 'react-native-collapsible'
import propTypes from 'prop-types'
import {Icon, Card} from 'react-native-elements'

// import { NavigationActions } from 'react-navigation'
import {Colors} from '../../Themes/'
import PMNavigationBar from '../../Components/Navbar'
import I18n from '../../I18n/I18n'

import Log from '../../Utils/Log'
const log = new Log('Containers/Settings/Settings')

const questions = []

for (let i = 1; i <= 6; i++) {
  questions.push({
    title: `Faq-Sleep.questions.${i}.title`,
    answer: `Faq-Sleep.questions.${i}.answer`
  })
}

class CollapsibleView extends Component {
  static propTypes = {
    title: propTypes.string
  }
  state = {
    collapsed: true
  }

  render () {
    const {title} = this.props
    return (
      <View>
        <TouchableOpacity onPress={() => this.setState({collapsed: !this.state.collapsed})} style={{flexDirection: 'row', alignItems: 'flex-start', marginRight: 15}}>
          <Icon name={this.state.collapsed ? 'chevron-right' : 'chevron-down'} type='font-awesome' containerStyle={{marginTop: 3, width: 15, marginRight: 5}} iconStyle={[styles.headline, {fontSize: 14}]} />
          <Text style={[styles.headline, {marginRight: 15}]}>{title}</Text>
        </TouchableOpacity>
        <Collapsible collapsed={this.state.collapsed}>
          {this.props.children}
        </Collapsible>
      </View>
    )
  }
}

class Sleep extends Component {
  state = {
    activeSection: false,
    collapsed: true
  }

  toggleExpanded = () => {
    this.setState({ collapsed: !this.state.collapsed })
  }

  setSection = section => {
    this.setState({ activeSection: section })
  }

  renderNavigationbar (props) {
    let title = I18n.t('Faq-Sleep.header')
    return (
      <PMNavigationBar title={title} props={props} rightButton={<View />} />
    )
  }

  render () {
    const { openURL } = this.props.screenProps
    return (
      <View style={styles.container}>
        {this.renderNavigationbar(this.props)}
        <ScrollView style={styles.content} indicatorStyle='white'>
          <Card
            title={I18n.t('Faq-Sleep.faqTitle')}
            titleStyle={styles.cardTitle}
            containerStyle={{marginBottom: 15}}
            >
            <View key={1}>
              {
                questions.map((question, i) =>
                  <CollapsibleView key={i} title={(i + 1) + '. ' + I18n.t(question.title)}>
                    <View style={{ flex: 1 }}>
                      <ParsedText
                        style={styles.paragraph}
                        parse={[{type: 'email', style: styles.url, onPress: (mail) => openURL('mailto:' + mail)},
                        {type: 'url', style: styles.url, onPress: (url) => openURL(url)}]}
                      >
                        {I18n.t(question.answer)}
                      </ParsedText>
                    </View>
                  </CollapsibleView>
                )
              }
            </View>
            <CollapsibleView title={I18n.t('Faq-Sleep.questions.7.title')}>
              <View style={{ flex: 1 }}>
                <TouchableOpacity onPress={() => Linking.openURL('https://science.sciencemag.org/content/294/5544/1048')}>
                  <Text style={{color: 'blue'}}>
                  [1]  P. Maquet, “The role of sleep in learning and memory,” Science, vol. 294, no. 5544. American Association for the Advancement of Science, pp. 1048–1052, 02-Nov-2001, doi: 10.1126/science.1062856.
                  </Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.margin} onPress={() => Linking.openURL('https://www.sleephealthjournal.org/article/S2352-7218(20)30132-7/pdf')}>
                  <Text style={{color: 'blue'}}>
                  [2]  M. I. Barber, “Sleep in a Time of Pandemic-A Position Statement from the National Sleep Foundation,” Sleep Heal.
                  </Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.margin} onPress={() => Linking.openURL('https://www.nature.com/articles/nrn2576')}>
                  <Text style={{color: 'blue'}}>
                  [3]  L. Imeri and M. R. Opp, “How (and why) the immune system makes us sleep,” Nature Reviews Neuroscience, vol. 10, no. 3. Nature Publishing Group, pp. 199–210, 11-Mar-2009, doi: 10.1038/nrn2576.
                  </Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.margin} onPress={() => Linking.openURL('https://www.ncbi.nlm.nih.gov/pmc/articles/PMC3669059/')}>
                  <Text style={{color: 'blue'}}>
                  [4]  P. K. Alvaro, R. M. Roberts, and J. K. Harris, “A Systematic Review Assessing Bidirectionality between Sleep Disturbances, Anxiety, and Depression,” Sleep, vol. 36, no. 7, pp. 1059–1068, Jul. 2013, doi: 10.5665/sleep.2810.
                  </Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.margin} onPress={() => Linking.openURL('https://www.jmir.org/2020/4/e15841/')}>
                  <Text style={{color: 'blue'}}>
                  [5]  N. R. Majd et al., “Efficacy of a Theory-Based Cognitive Behavioral Technique App-Based Intervention for Patients With Insomnia: Randomized Controlled Trial,” J. Med. Internet Res., vol. 22, no. 4, p. e15841, 2020.
                  </Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.margin} onPress={() => Linking.openURL('https://applications.emro.who.int/imemrf/Gulf_Med_Univ_Proc/Gulf_Med_Univ_Proc_2014_5-6_6_15.pdf')}>
                  <Text style={{color: 'blue'}}>
                  [6]  B. D. Akçay, D. Akçay, and S. Yetkin, “The effects of mobile electronic devices use on the sleep states of university students,” Anatol. J. Psychiatry, vol. 21, p. 0, 2020.
                  </Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.margin} onPress={() => Linking.openURL('https://www.sciencedirect.com/science/article/pii/S1087079207000433?casa_token=KEhDzlJPOVEAAAAA:yu9Ml7eBd0-HW81a1ZOBcjXvYUH9bxjt75sRedH9hO7tZHFhXj53ZD1IvWTwxbcktdd8WjMohN95')}>
                  <Text style={{color: 'blue'}}>
                  [7]  R. V. T. Santos, S. Tufik, and M. T. De Mello, “Exercise, sleep and cytokines: Is there a relation?,” Sleep Medicine Reviews, vol. 11, no. 3. W.B. Saunders, pp. 231–239, 01-Jun-2007, doi: 10.1016/j.smrv.2007.03.003.
                  </Text>
                </TouchableOpacity>
              </View>
            </CollapsibleView>
          </Card>
        </ScrollView>
      </View>
    )
  }
}

export default Sleep

const styles = StyleSheet.create({
  url: {
    color: Colors.buttons.common.background
  },
  headline: {
    color: Colors.main.headline,
    fontWeight: 'bold',
    marginTop: 20,
    marginBottom: 10,
    flexWrap: 'wrap'
  },
  container: {
    flex: 1,
    backgroundColor: Colors.main.appBackground
  },
  cardTitle: {
    textAlign: 'left',
    color: Colors.main.headline
  },
  content: {
    flex: 1
  },
  margin: {
    marginTop: 15,
  }
})
