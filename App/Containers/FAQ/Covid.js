
// TODO for improvement check: https://github.com/idibidiart/react-native-responsive-grid/blob/master/UniversalTiles.md

import React, { Component } from 'react'
import { Text, View, StyleSheet, ScrollView, TouchableOpacity, Linking } from 'react-native'
import ParsedText from 'react-native-parsed-text'
import Collapsible from 'react-native-collapsible'
import propTypes from 'prop-types'
import {Icon, Card} from 'react-native-elements'

// import { NavigationActions } from 'react-navigation'
import {Colors} from '../../Themes/'
import PMNavigationBar from '../../Components/Navbar'
import I18n from '../../I18n/I18n'

import Log from '../../Utils/Log'
const log = new Log('Containers/Settings/Settings')

const questions = []

for (let i = 1; i <= 10; i++) {
  questions.push({
    title: `Covid.questions.${i}.title`,
    answer: `Covid.questions.${i}.answer`
  })
}

class CollapsibleView extends Component {
  static propTypes = {
    title: propTypes.string
  }
  state = {
    collapsed: true
  }

  render () {
    const {title} = this.props
    return (
      <View>
        <TouchableOpacity onPress={() => this.setState({collapsed: !this.state.collapsed})} style={{flexDirection: 'row', alignItems: 'flex-start', marginRight: 15}}>
          <Icon name={this.state.collapsed ? 'chevron-right' : 'chevron-down'} type='font-awesome' containerStyle={{marginTop: 3, width: 15, marginRight: 5}} iconStyle={[styles.headline, {fontSize: 14}]} />
          <Text style={[styles.headline, {marginRight: 15}]}>{title}</Text>
        </TouchableOpacity>
        <Collapsible collapsed={this.state.collapsed}>
          {this.props.children}
        </Collapsible>
      </View>
    )
  }
}

class Covid extends Component {
  state = {
    activeSection: false,
    collapsed: true
  }

  toggleExpanded = () => {
    this.setState({ collapsed: !this.state.collapsed })
  }

  setSection = section => {
    this.setState({ activeSection: section })
  }

  renderNavigationbar (props) {
    let title = I18n.t('Covid.header')
    return (
      <PMNavigationBar title={title} props={props} rightButton={<View />} />
    )
  }

  render () {
    const { openURL } = this.props.screenProps
    return (
      <View style={styles.container}>
        {this.renderNavigationbar(this.props)}
        <ScrollView style={styles.content} indicatorStyle='white'>
          <Card
            title={I18n.t('Covid.covidTitle')}
            titleStyle={styles.cardTitle}
            containerStyle={{marginBottom: 15}}
            >
            <View key={1}>
              {
                questions.map((question, i) =>
                  <CollapsibleView key={i} title={(i + 1) + '. ' + I18n.t(question.title)}>
                    <View style={{ flex: 1 }}>
                      <ParsedText
                        style={styles.paragraph}
                        parse={[{type: 'email', style: styles.url, onPress: (mail) => openURL('mailto:' + mail)},
                        {type: 'url', style: styles.url, onPress: (url) => openURL(url)}]}
                      >
                        {I18n.t(question.answer)}
                      </ParsedText>  
                    </View>
                  </CollapsibleView>
                )
              }
            </View>
            <CollapsibleView title={I18n.t('Covid.questions.11.title')}>
              <View style={{ flex: 1 }}>
                <TouchableOpacity onPress={() => Linking.openURL('https://pubmed.ncbi.nlm.nih.gov/32112977/')}>
                  <Text style={{color: 'blue'}}>
                  [1]  C. Sohrabi et al., ‘World Health Organization declares global emergency: A review of the 2019 novel coronavirus (COVID-19),' International Journal of Surgery, vol. 76. Elsevier Ltd, pp. 71–76, 01-Apr-2020, doi: 10.1016/j.ijsu.
                  </Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.margin} onPress={() => Linking.openURL('https://www.sciencedirect.com/science/article/pii/S1201971220301363')}>
                  <Text style={{color: 'blue'}}>
                  [2]  J. Yang et al., ‘Prevalence of comorbidities and its effects in patients infected with SARS-CoV-2: a systematic review and meta-analysis,' 2020, doi: 10.1016/j.ijid.2020.03.017.
                  </Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.margin} onPress={() => Linking.openURL('https://pubmed.ncbi.nlm.nih.gov/32220574/')}>
                  <Text style={{color: 'blue'}}>
                  [3]  W.-H. Hsih et al., ‘Featuring COVID-19 cases via screening symptomatic patients with epidemiologic link during flu season in a medical center of central Taiwan KEYWORDS,' Immunol. Infect., vol. 53, pp. 459–466, 2020, doi: 10.1016/j.jmii.2020.03.008.
                  </Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.margin} onPress={() => Linking.openURL('https://www.ncbi.nlm.nih.gov/books/NBK554776/')}>
                  <Text style={{color: 'blue'}}>
                  [4]  M. Cascella, M. Rajnik, A. Cuomo, S. C. Dulebohn, and R. Di Napoli, Features, Evaluation and Treatment Coronavirus (COVID-19). StatPearls Publishing, 2020.
                  </Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.margin} onPress={() => Linking.openURL('https://www.who.int/gpsc/5may/en/')}>
                  <Text style={{color: 'blue'}}>
                  [5]  C. Kilpatrick and D. Pittet, ‘WHO SAVE LIVES: Clean Your Hands global annual campaign. A call for action: 5 May 2011.' Springer, 2011.
                  </Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.margin} onPress={() => Linking.openURL('https://www.bmj.com/content/369/bmj.m1873')}>
                  <Text style={{color: 'blue'}}>
                  [6]  R. E. Jordan, P. Adab, and K. K. Cheng, ‘Covid-19: risk factors for severe disease and death.' British Medical Journal Publishing Group, 2020.
                  </Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.margin} onPress={() => Linking.openURL('https://www.ncbi.nlm.nih.gov/pmc/articles/PMC7093856/')}>
                  <Text style={{color: 'blue'}}>
                  [7]  S. A. Rasmussen, J. C. Smulian, J. A. Lednicky, T. S. Wen, and D. J. Jamieson, ‘Coronavirus Disease 2019 (COVID-19) and pregnancy: what obstetricians need to know,' American Journal of Obstetrics and Gynecology, vol. 222, no. 5. Mosby Inc., pp. 415–426, 01-May-2020, doi: 10.1016/j.ajog.2020.02.017.
                  </Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.margin} onPress={() => Linking.openURL('https://jamanetwork.com/journals/jama/fullarticle/2763533')}>
                  <Text style={{color: 'blue'}}>
                  [8]  A. N. Desai and P. Patel, ‘Stopping the Spread of COVID-19,' JAMA - Journal of the American Medical Association, vol. 323, no. 15. American Medical Association, p. 1516, 21-Apr-2020, doi: 10.1001/jama.2020.4269.
                  </Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.margin} onPress={() => Linking.openURL('https://www.bmj.com/content/369/bmj.m2010')}>
                  <Text style={{color: 'blue'}}>
                  [9]  R. C. Schroter, ‘Social distancing for covid-19: Is 2 metres far enough?,' The BMJ, vol. 369. BMJ Publishing Group, 21-May-2020, doi: 10.1136/bmj.m2010.
                  </Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.margin} onPress={() => Linking.openURL('https://www.who.int/bulletin/volumes/89/7/11-088815/en/')}>
                  <Text style={{color: 'blue'}}>
                  [10]  H. Kelly, ‘The classical definition of a pandemic is not elusive,' Bull. World Health Organ., vol. 89, pp. 540–541, 2011.
                  </Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.margin} onPress={() => Linking.openURL('https://academic.oup.com/cid/article/47/5/668/296225')}>
                  <Text style={{color: 'blue'}}>
                  [11]  A. Trilla, G. Trilla, and C. Daer, ‘The 1918 ‘Spanish Flu' in Spain,' Clin. Infect. Dis., vol. 47, no. 5, pp. 668–673, Sep. 2008, doi: 10.1086/590567.
                  </Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.margin} onPress={() => Linking.openURL('https://www.thelancet.com/pdfs/journals/lancet/PIIS0140-6736(20)30567-5.pdf')}>
                  <Text style={{color: 'blue'}}>
                  [12]  R. M. Anderson, H. Heesterbeek, D. Klinkenberg, and T. D. Hollingsworth, ‘How will country-based mitigation measures influence the course of the COVID-19 epidemic?,' The Lancet, vol. 395, no. 10228. Lancet Publishing Group, pp. 931–934, 21-Mar-2020, doi: 10.1016/S0140-6736(20)30567-5.
                  </Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.margin} onPress={() => Linking.openURL('https://papers.ssrn.com/sol3/papers.cfm?abstract_id=3563984')}>
                  <Text style={{color: 'blue'}}>
                  [13]  G. Briscese, N. Lacetera, M. Macis, and M. Tonin, ‘Compliance with COVID-19 Social-Distancing Measures in Italy: The Role of Expectations and Duration,' 2020.
                  </Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.margin} onPress={() => Linking.openURL('https://smw.ch/article/doi/smw.2020.20225')}>
                  <Text style={{color: 'blue'}}>
                  [14]  S. Marcel et al., ‘COVID-19 epidemic in Switzerland: on the importance of testing, contact tracing and isolation,' doi: 10.4414/smw.2020.20225.
                  </Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.margin} onPress={() => Linking.openURL('https://pubmed.ncbi.nlm.nih.gov/32193299/')}>
                  <Text style={{color: 'blue'}}>
                  [15]  J. Cohen and K. Kupferschmidt, ‘Countries test tactics in ‘war'against COVID-19.' American Association for the Advancement of Science, 2020.
                  </Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.margin} onPress={() => Linking.openURL('https://apps.who.int/iris/handle/10665/332071')}>
                  <Text style={{color: 'blue'}}>
                  [16]  World Health Organization, ‘Protocol for assessment of potential risk factors for coronavirus disease 2019 (COVID-19) among health workers in a health care setting, 23 March 2020,' World Health Organization, 2020.
                  </Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.margin} onPress={() => Linking.openURL('https://www.sciencedirect.com/science/article/pii/S209379112030161X')}>
                  <Text style={{color: 'blue'}}>
                  [17]  W. H. Gan, J. W. Lim, and D. Koh, ‘Preventing Intra-hospital Infection and Transmission of Coronavirus Disease 2019 in Health-care Workers,' Saf. Health Work, vol. 11, no. 2, pp. 241–243, Jun. 2020, doi: 10.1016/j.shaw.2020.03.001.
                  </Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.margin} onPress={() => Linking.openURL('https://apps.who.int/iris/handle/10665/332293')}>
                  <Text style={{color: 'blue'}}>
                  [18]  World Health Organization, ‘Advice on the use of masks in the context of COVID-19: interim guidance, 5 June 2020,' World Health Organization, 2020.
                  </Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.margin} onPress={() => Linking.openURL('https://www.ncbi.nlm.nih.gov/pmc/articles/PMC7118603/')}>
                  <Text style={{color: 'blue'}}>
                  [19]  S. Feng, C. Shen, N. Xia, W. Song, M. Fan, and B. J. Cowling, ‘Rational use of face masks in the COVID-19 pandemic,' The Lancet Respiratory Medicine, vol. 8, no. 5. Lancet Publishing Group, pp. 434–436, 01-May-2020, doi: 10.1016/S2213-2600(20)30134-X.
                  </Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.margin} onPress={() => Linking.openURL('https://www.sciencedirect.com/science/article/pii/S2665910720300396')}>
                  <Text style={{color: 'blue'}}>
                  [20]  N. M. A. Parry, ‘COVID-19 and pets: When pandemic meets panic,' Forensic Sci. Int. Reports, vol. 2, p. 100090, Dec. 2020, doi: 10.1016/j.fsir.2020.100090.
                  </Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.margin} onPress={() => Linking.openURL('https://veterinaryrecord.bmj.com/content/186/12/388.2')}>
                  <Text style={{color: 'blue'}}>
                  [21]  A. Almendros, ‘Can companion animals become infected with Covid-19?,' Veterinary Record, vol. 186, no. 12. British Veterinary Association, pp. 388–389, 28-Mar-2020, doi: 10.1136/vr.m1194.
                  </Text>
                </TouchableOpacity>
              </View>
            </CollapsibleView>
          </Card>
        </ScrollView>
      </View>
    )
  }
}

export default Covid

const styles = StyleSheet.create({
  url: {
    color: Colors.buttons.common.background
  },
  headline: {
    color: Colors.main.headline,
    fontWeight: 'bold',
    marginTop: 20,
    marginBottom: 10,
    flexWrap: 'wrap'
  },
  container: {
    flex: 1,
    backgroundColor: Colors.main.appBackground
  },
  cardTitle: {
    textAlign: 'left',
    color: Colors.main.headline
  },
  content: {
    flex: 1
  },
  margin: {
    marginTop: 15,
  }
})
